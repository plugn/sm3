function realMetrics () {

	var bbox3 = {
		lat: 64.68632,
		lng: 97.74531,
		nw_lat: 81.413933,
		nw_lng: -25.3125,
		se_lat: 22.43134,
		se_lng: 220.429688
	};

	var bbox7 = {
		lat: 55.504314,
		lng: 38.035393,
		nw_lat: 57.780376,
		nw_lng: 29.992676,
		se_lat: 53.357109,
		se_lng: 45.351563
	};

	var bbox10 = {
		lat: 55.79769,
		lng: 38.66543,
		nw_lat: 56.072035,
		nw_lng: 37.703705,
		se_lat: 55.521634,
		se_lng: 39.623566
	};

	var bbox11 = {
		lng: 55.947727,
		lat: 54.726288,
		nw_lat: 54.875817,
		nw_lng: 55.463791,
		se_lat: 54.57644,
		se_lng: 56.431274
	};
	var bbox13 = {
		lat: 55.849472,
		lng: 37.511852,
		nw_lat: 55.886673,
		nw_lng: 37.38905,
		se_lat: 55.812279,
		se_lng: 37.634697
	};
	var bbox16 = {
		lat: 55.88899,
		lng: 37.597076,
		nw_lat: 55.893532,
		nw_lng: 37.581954,
		se_lat: 55.884446,
		se_lng: 37.612188
	};

	var bbox18 = {
		lat: 55.88899,
		lng: 37.597076,
		nw_lat: 55.890126,
		nw_lng: 37.593294,
		se_lat: 55.887855,
		se_lng: 37.600853
	};

	return {
		'country':  bbox3,
		'region':  bbox7,
		'district': bbox10,
		'place': bbox11,
		'admin': bbox13,
		'street': bbox16,
		'subway': bbox16,
		'railway_station': bbox16,
		'house': bbox18
	}
}

function getMean() {
	var args = [].slice.call(arguments);
	var sum = args.reduce(function(acc, val){
		return acc + val;
	}, 0);
	return sum / args.length;
}

function calcBbox(zoom) {
	var metrics = realMetrics(),
		bbox = metrics[zoom];

	var nwLatD = bbox.nw_lat - bbox.lat,
		seLatD = bbox.lat - bbox.se_lat,
		nwLngD = bbox.lng - bbox.nw_lng,
	 	seLngD = bbox.se_lng - bbox.lng;
    return {
		latD: +(getMean(nwLatD, seLatD).toFixed(8)),
		lngD: +(getMean(nwLngD, seLngD).toFixed(8))
	};
}

function getDeltas() {
	var zooms = Object.keys(realMetrics());
	return zooms.reduce(
		function(acc, zoom) {
			acc[zoom] = calcBbox(zoom);
			return acc;
		},
	{});
}