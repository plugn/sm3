# JavaScript API Спутник/Карты

## Геокодер

Для поиска объектов на карте можно использовать контрол `L.sm.geocoder()`

```html
<div id="map" class="user-map map"></div>

<script>
	var map = L.sm.map('map', {center: [55.775, 37.624], zoom: 12});
	L.sm.geocoder().addTo(map);
</script>
```

<div id="map" class="user-map map"></div>

<script>
	var map = L.sm.map('map', {center: [55.775, 37.624], zoom: 12});
		L.sm.geocoder({
			smMarkerOptions: {
				type: 'location-firebrick' // можно указать тип маркера для результата поиска
			}
		}
	).addTo(map);
</script>


<a href="./examples/geocoder.html" target="_blank">Пример на отдельной странице</a>  

## Дополнительная информация

*   <a href="https://github.com/Leaflet/Leaflet.markercluster" target="_blank">Документация L.MarkerCluster</a>
*   [Введение](./index.html)
*   [Карты](./map.html)
*   [Маркеры](./marker.html)
*   [Кластеры маркеров](./cluster.html)
*   [Попапы](./popup.html)
*   [Отображение геометрии](./geojson.html)

