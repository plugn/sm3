# JavaScript API Спутник/Карты

## Создание карты

Подготовьте страницу, как описано во [Введении](./index.html). 
Добавьте в HTML-код контейнер карты -- элемент `div` и задайте ему атрибут `id`. 
Теперь нужно инициализировать карту вызовом L.sm.map(`id_контейнера`).  

```html
<div id="map1"></div>
<script>
	L.sm.map('map1');
</script>
```

<div id="map1" class="map"></div>
<script>
	L.sm.map('map1');
</script>


## Создание нескольких карт

Несколько независимых карт создается аналогичным образом.
 
```html
<div id="map2" class="map"></div>
<div id="map3" class="map"></div>
<script>
	L.sm.map('map2');
	L.sm.map('map3');
</script>
```

<div id="map2" class="map"></div>
<div id="map3" class="map"></div>
<script>
	L.sm.map('map2');
	L.sm.map('map3');
</script>


## Копирайт 

Вы можете добавить на карту данные копирайта. Для этого используйте метод карты `addAttribution(html)`:

```html
<div id="map4"></div>
<script>
	var map4 = L.sm.map('map1');
	map4.attributionControl.addAttribution('<a href="http://dom.sputnik.ru/">&copy; Мой Дом</a>');
	map4.attributionControl.addAttribution('Traffic data &copy; GeoTraffic');
</script>
```

<div id="map4" class="map"></div>
<script>
	var map4 = L.sm.map('map4');
	map4.attributionControl.addAttribution('<a href="http://dom.sputnik.ru/">&copy; Мой Дом</a>');
	map4.attributionControl.addAttribution('Traffic data &copy; GeoTraffic');
</script>


<a href="./examples/map.html" target="_blank">Пример на отдельной странице</a>

## Дополнительная информация

*   [Введение](./index.html)
*   [Маркеры](./marker.html)
*   [Попапы](./popup.html)
*   [Отображение геометрии](./geojson.html)
*   [Кластеры маркеров](./cluster.html)
*   [Геокодер](./geocoder.html)
*   <a href="http://leafletjs.com/reference.html" target="_blank">Документация LeafletJS</a>
