# JavaScript API Спутник/Карты

## Кластеры маркеров

В случае, когда количество отображаемых маркеров слишком велико, существует возможность объединять их в кластеры. 
Кластеризация реализована с помощью плагина <a href="https://github.com/Leaflet/Leaflet.markercluster" target="_blank">L.MarkerCluster</a>.

	
Пример:     

```html
<div id="map1" class="map"></div>

<script>
	var map1 = L.sm.map('map1', {center: [55.775, 37.624], zoom: 9});	// создаем карту
	var options = {maxClusterRadius: 70};	// параметры кластера
	var cluster = L.sm.cluster(options);	// создаем кластер

	var options = {
		popupTemplate: '<b>{title}</b>',       // можно использовать шаблон попапа для всех элементов GeoJson
		defaultPopupTemplate:'<u>{title}</u>'  // или шаблон попапа для элементов, у которых нет popupTemplate
	};

	var geoJsonLayer = L.sm.geoJson(geoData1, options); // создаем слой данных из GeoJSON

	cluster.addLayer(geoJsonLayer);		// добавляем в кластер слой данных
	cluster.addTo(map1); 				// добавляем кластер на карту
</script>
```

<div id="map1" class="map"></div>

<script>
	var map1 = L.sm.map('map1', {center: [55.775, 37.624], zoom: 9}); 		// создаем карту
	var options = {maxClusterRadius: 70};	// параметры кластера
	var cluster = L.sm.cluster(options);	// создаем кластер

	var options = {
		popupTemplate: '<b>{title}</b>',       // шаблон попапа для всех элементов GeoJson
		defaultPopupTemplate:'<u>{title}</u>'  // шаблон попапа для элементов, у которых нет popupTemplate
	};

	var geoJsonLayer = L.sm.geoJson(geoData1, options); // создаем слой данных из GeoJSON

	cluster.addLayer(geoJsonLayer);		// добавляем в кластер слой данных
	cluster.addTo(map1); 				// добавляем кластер на карту
</script>

<a href="./examples/cluster.html" target="_blank">Пример на отдельной странице</a>  
<a target="_blank" href="./examples/clusters_data.js">Пример данных</a>

## Дополнительная информация

*   <a href="https://github.com/Leaflet/Leaflet.markercluster" target="_blank">Документация L.MarkerCluster</a>
*   [Введение](./index.html)
*   [Карты](./map.html)
*   [Маркеры](./marker.html)
*   [Попапы](./popup.html)
*   [Отображение геометрии](./geojson.html)
*   [Геокодер](./geocoder.html)

