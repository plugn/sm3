# JavaScript API Спутник/Карты

## Добавить маркер

Добавить одиночный маркер можно одним из следующих способов:

```html
<div id="map1"></div>

<script>
var map1 = L.sm.map('map1'); // создаем карту

var myMarker1 = L.sm.marker([55.55, 37.6167]); // создаем маркер с координатами
map1.addLayer(myMarker1); // первый способ: карта добавляет маркер 

var myMarker2 = L.sm.marker([55.65, 37.6167], {type: 'location-firebrick'});
myMarker2.addTo(map1); //  второй способ: маркер добавляет себя на карту 
</script>
```
<div id="map1" class="map"></div>

<script>
var map1 = L.sm.map('map1'); // создаем карту

var myMarker1 = L.sm.marker([55.55, 37.6167]); // создаем маркер с координатами
map1.addLayer(myMarker1); // первый способ: карта добавляет маркер 

var myMarker2 = L.sm.marker([55.65, 37.6167], {type: 'location-firebrick'});
myMarker2.addTo(map1); //  второй способ: маркер добавляет себя на карту 
</script>

Используя параметр **`type`**, можно выбрать один из следующих видов маркера:

type | маркер | type | маркер
--- | -- | --- | ---
`default` | ![](http://js-api.maps.sputnik.ru/v0.2/dist/themes/sputnik_maps/images/default.png) | `alt1` | ![](http://js-api.maps.sputnik.ru/v0.2/dist/themes/sputnik_maps/images/alt1.png)
`alt2` | ![](http://js-api.maps.sputnik.ru/v0.2/dist/themes/sputnik_maps/images/alt2.png) | `alt3` | ![](http://js-api.maps.sputnik.ru/v0.2/dist/themes/sputnik_maps/images/alt3.png)
`location-cfblue` | ![](http://js-api.maps.sputnik.ru/v0.3/styles/images/location-cfblue.svg "location-cfblue") | `target-cfblue` | ![](http://js-api.maps.sputnik.ru/v0.3/styles/images/target-cfblue.svg "target-cfblue")
`location-chocolate` | ![](http://js-api.maps.sputnik.ru/v0.3/styles/images/location-chocolate.svg "location-chocolate") | `target-chocolate` | ![](http://js-api.maps.sputnik.ru/v0.3/styles/images/target-chocolate.svg "target-chocolate")
`location-coral` | ![](http://js-api.maps.sputnik.ru/v0.3/styles/images/location-coral.svg "location-coral") | `target-coral` | ![](http://js-api.maps.sputnik.ru/v0.3/styles/images/target-coral.svg "target-coral")
`location-darkcyan` | ![](http://js-api.maps.sputnik.ru/v0.3/styles/images/location-darkcyan.svg "location-darkcyan") | `target-darkcyan` | ![](http://js-api.maps.sputnik.ru/v0.3/styles/images/target-darkcyan.svg "target-darkcyan")
`location-darkorange` | ![](http://js-api.maps.sputnik.ru/v0.3/styles/images/location-darkorange.svg "location-darkorange") | `target-darkorange` | ![](http://js-api.maps.sputnik.ru/v0.3/styles/images/target-darkorange.svg "target-darkorange")
`location-firebrick` | ![](http://js-api.maps.sputnik.ru/v0.3/styles/images/location-firebrick.svg "location-firebrick") | `target-firebrick` | ![](http://js-api.maps.sputnik.ru/v0.3/styles/images/target-firebrick.svg "target-firebrick")
`location-lawngreen` | ![](http://js-api.maps.sputnik.ru/v0.3/styles/images/location-lawngreen.svg "location-lawngreen") | `target-lawngreen` | ![](http://js-api.maps.sputnik.ru/v0.3/styles/images/target-lawngreen.svg "target-lawngreen")
`location-mediumblue` | ![](http://js-api.maps.sputnik.ru/v0.3/styles/images/location-mediumblue.svg "location-mediumblue") | `target-mediumblue` | ![](http://js-api.maps.sputnik.ru/v0.3/styles/images/target-mediumblue.svg "target-mediumblue")
`location-seagreen` | ![](http://js-api.maps.sputnik.ru/v0.3/styles/images/location-seagreen.svg "location-seagreen") | `target-seagreen` | ![](http://js-api.maps.sputnik.ru/v0.3/styles/images/target-seagreen.svg "target-seagreen")
`location-skyblue` | ![](http://js-api.maps.sputnik.ru/v0.3/styles/images/location-skyblue.svg "location-skyblue") | `target-skyblue` | ![](http://js-api.maps.sputnik.ru/v0.3/styles/images/target-skyblue.svg "target-skyblue")
`location-steelblue` | ![](http://js-api.maps.sputnik.ru/v0.3/styles/images/location-steelblue.svg "location-steelblue") | `target-steelblue` | ![](http://js-api.maps.sputnik.ru/v0.3/styles/images/target-steelblue.svg "target-steelblue")
`location-tomato` | ![](http://js-api.maps.sputnik.ru/v0.3/styles/images/location-tomato.svg "location-tomato") | `target-tomato` | ![](http://js-api.maps.sputnik.ru/v0.3/styles/images/target-tomato.svg "target-tomato")

### URL иконки маркера

**`iconUrl`** = 'path/to/image.png'

### Размер иконки
 
При необходимости можно указать размеры иконки в пикселах:  
**`iconSize`** = [<ширина>, <высота>],  

либо имя CSS-класса для иконки:  
**`iconClassName`** = 'icon-w25h41'. 

```html
<style>
.icon-w25h41 {
	width: 25px;
	height: 41px;
}
</style>
<div id="map2"></div> 
<script>
var map2 = L.sm.map('map2'); // создаем карту

var myMarker21 = L.sm.marker([55.85, 37.61], {
	iconUrl: '//maps-js.apissputnik.ru/v0.3/images/marker-icon.png',
	iconSize: [38, 61] // размер иконки
});
myMarker21.addTo(map2);

var myMarker22 = L.sm.marker([55.65, 37.67], {
	iconUrl: '//maps-js.apissputnik.ru/v0.3/images/marker-icon-2x.png',
	iconClassName: 'icon-w25h41' // CSS-класс иконки
});
myMarker22.addTo(map2);
</script>
```

<style>
.icon-w25h41 {
	width: 25px;
	height: 41px;
}
</style>

<div id="map2" class="map">
</div>
 
<script>
var map2 = L.sm.map('map2'); // создаем карту

var myMarker3 = L.sm.marker([55.85, 37.61], {
	iconUrl: '//maps-js.apissputnik.ru/v0.3/images/marker-icon-2x.png',
	iconSize: [38, 61]
});
myMarker3.addTo(map2); // идентично map2.addLayer(myMarker3)
var myMarker22 = L.sm.marker([55.65, 37.67], {
	iconUrl: '//maps-js.apissputnik.ru/v0.3/images/marker-icon-2x.png',
	iconClassName: 'icon-w25h41' // CSS-класс иконки
});
myMarker22.addTo(map2);

</script>



###   Класс маркера  
*    **`className`**  --  атрибут `class` DOM-элемента маркера  

Полный список параметров иконки <a href="http://leafletjs.com/reference.html#icon-options" target="_blank">в документации Leaflet</a>.


###  Полный контроль над иконками

Описанный выше способ работы с иконками подходит для простых задач. В случаях, когда нужна ссылка на объект иконки,
используйте конструктор иконок **`L.icon()`**, такой объект можно передать маркеру в параметре **`icon`**.

```html
<div id="map3"></div> 
<script>
var map3 = L.sm.map('map3'); // создаем карту

var myIcon3 = L.icon({ // создаем иконку
	iconUrl: '//maps-js.apissputnik.ru/v0.3/images/marker-icon.png'
});

var myMarker30 = L.sm.marker([55.85, 37.61], {
	icon: myIcon3 // передаем иконку маркеру
});

myMarker30.addTo(map3); // добавляем маркер на карту

</script>
```

<div id="map3" class="map"></div> 
<script>
var map3 = L.sm.map('map3'); // создаем карту
var myIcon3 = L.icon({
	iconUrl: '//maps-js.apissputnik.ru/v0.3/images/marker-icon.png'
});

var myMarker30 = L.sm.marker([55.85, 37.61], {
	icon: myIcon3
});
myMarker30.addTo(map3); // идентично map2.addLayer(myMarker3)

</script>

<a href="./examples/marker.html" target="_blank">Пример на отдельной странице</a>

## Дополнительная информация

*   [Введение](./index.html)
*   [Карты](./map.html)
*   [Попапы](./popup.html)
*   [Отображение геометрии](./geojson.html)
*   [Кластеры маркеров](./cluster.html)
*   [Геокодер](./geocoder.html)
*   <a href="http://leafletjs.com/reference.html" target="_blank">Документация LeafletJS</a>
