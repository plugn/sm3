# JavaScript API Спутник/Карты

JavaScript API Спутник/Карт позволяет легко вставить карту на свой сайт, добавить маркеры и дополнительную
	информацию.

## Подготовка

Ознакомьтесь с <a href="http://corp.sputnik.ru/maps" target="_blank">Пользовательским Соглашением</a>. 
Для ресурсов с высокой посещаемостью рекомендуется использовать API-ключ. 
Для получении API-ключа необходимо связаться со службой поддержки по <a href="mailto:maps@corp.sputnik.ru">электронной почте</a>.

Для работы с API Спутник.Карт нужно проделать несколько шагов:  
Подключить файлы CSS и JS в секции `head`:

```html
<head>
	<link rel="stylesheet" href="//maps-js.apissputnik.ru/v0.3/sputnik_maps_bundle.min.css" />
	<script src="//maps-js.apissputnik.ru/v0.3/sputnik_maps_bundle.min.js"></script>
</head>
```

Поместить в тело документа `body` контейнер карты `div` с установленным свойством `id`.

Задать высоту контейнеру карты, указав CSS-свойство `height`.

Инициализировать карту, добавив код в тэге  `<script>` в конец `<body>`.
  
```html
<!doctype html>
<html>
<head>
	<link href="//maps-js.apissputnik.ru/v0.3/sputnik_maps_bundle.min.css" rel="stylesheet" />
	<script src="//maps-js.apissputnik.ru/v0.3/sputnik_maps_bundle.min.js"></script>
</head>
<body>
	<!--
	Поместить в тело документа `body` контейнер карты `div` с установленным свойством `id`.
	Задать высоту контейнеру карты, указав CSS-свойство `height`.
	-->
	<div id="map1" style="height: 400px;"></div>
		
	<!--Инициализировать карту-->
	<script>
	L.sm.apiKey = '<Ваш API-ключ>';
	var map = L.sm.map('map1');
	</script>
</body>
</html>
```

Будет создана карта с центром в точке c координатами по умолчанию [55.75, 37.6167] (Москва), 9-й зум.	

<div id="map1" class="map"></div>
<script>
	var map = L.sm.map('map1');
</script>

Детальную информацию о параметрах можно посмотреть в разделе [Карты](./map.html).


## Дополнительная информация

*   [Карты](./map.html)
*   [Маркеры](./marker.html)
*   [Попапы](./popup.html)
*   [Отображение геометрии](./geojson.html)
*   [Кластеры маркеров](./cluster.html)
*   [Геокодер](./geocoder.html)
*   <a href="http://leafletjs.com/reference.html" target="_blank">Документация LeafletJS</a>
*   [Использование тайлов Спутник/Карт без JS API](http://api.sputnik.ru/maps/tiles)