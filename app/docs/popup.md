# JavaScript API Спутник/Карты

## Создание попапа для маркера

**`Попап`** - всплывающая плашка с информацией об объекте на карте. Попап привязывается к [**`маркеру`**](./marker.html) и изредка к точке на карте.  

Первый способ сделать это:  

```html
<div id="map2" class="map"></div>
<script>
var map2 = L.sm.map('map2'); // создаем карту
var marker2 = L.sm.marker([55.85, 37.57]); // создаем маркер
var popupContent = '<div class="my-popup-title"><h3>Привет</h3></div>'+
	'<div class="my-popup-description">Я попап маркера. Меня можно открыть и закрыть.</div>'; 
 
marker2.addTo(map2);
marker2.bindPopup(popupContent); 
marker2.openPopup(); // откроем попап

setTimeout(function(){   
	marker2.closePopup();  // закроем попап через 5 секунд
}, 5000);

</script>
```

<div id="map2" class="map"></div>
<script>
var map2 = L.sm.map('map2'); // создаем карту
var marker2 = L.sm.marker([55.85, 37.57]); // создаем маркер
var popupContent = '<div class="my-popup-title"><h3>Привет</h3></div><div class="my-popup-description">Я попап маркера. Меня можно открыть и закрыть.</div>';
 
marker2.addTo(map2);
marker2.bindPopup(popupContent); 
marker2.openPopup(); // откроем попап

setTimeout(function(){   
	marker2.closePopup();  // закроем попап через 5 секунд
}, 5000);

</script>

  
И более короткий способ, но без возможности управлять попапом в дальнейшем:  

```html
<div id="map1"></div>
<script>
var map1 = L.sm.map('map1'); // создаем карту
var marker1 = L.sm.marker([55.75, 37.8167], { // создаем маркер с раскрытым попапом
	popup: 'Привет!', // здесь может быть код HTML или DOM-элемент 
	popupOptions: {
		open: true 
	}
});
marker1.addTo(map1);
</script>
```

<div id="map1" class="map"></div>
<script>
var map1 = L.sm.map('map1'); // создаем карту
var marker1 = L.sm.marker([55.75, 37.8167], {
	popup: 'Привет!',
	popupOptions: {
		open: true
	}
});
marker1.addTo(map1);
</script>

Документация прочих опций попапа <a href="http://leafletjs.com/reference.html#popup-options" target="_blank">здесь</a>.
	
<a href="./examples/popup.html" target="_blank">Пример на отдельной странице</a>


## Дополнительная информация

*   [Введение](./index.html)
*   [Карты](./map.html)
*   [Маркеры](./marker.html)
*   [Отображение геометрии](./geojson.html)
*   [Кластеры маркеров](./cluster.html)
*   [Геокодер](./geocoder.html)
*   <a href="http://leafletjs.com/reference.html" target="_blank">Документация LeafletJS</a>
