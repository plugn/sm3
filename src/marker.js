var L = require('./leaflet');
require('./icon');

/**
 * appends to L.Marker and provides support for
 *
 * - options.iconUrl
 * - options.iconRetinaUrl
 *
 */
var Marker = L.Marker.extend({
	options: {
		definedColors: 'cfblue|chocolate|coral|darkcyan|darkorange|firebrick|lawngreen|mediumblue|seagreen|skyblue|steelblue|tomato'
	},

	initialize: function (latlng, options) {
		this._prepareIcon(options);
		L.Marker.prototype.initialize.call(this, latlng, options);
	},

	onAdd: function (map) {
		L.Marker.prototype.onAdd.call(this, map);

		if (this.options && this.options.popup) {
			this.bindPopup(this.options.popup, this.options.popupOptions || {});
			if (this.options.popupOptions && this.options.popupOptions.open) {
				this.openPopup();
			}
		}
	},

	_prepareIcon: function (options) {
		if (options && options.icon) { return; }

		var type = options && (options.type || options.iconType || options.markerType);
		var parts = new RegExp('^(location|target)-(' + this.options.definedColors + ')$').exec(type);
		var iconOptions = {};
		if (options && options.iconUrl) {
			for (var prop in options) {
				if ('iconClassName' === prop) {
					iconOptions.className = options[prop];
				} else if (/^icon|shadow|popup/.test(prop)) {
					iconOptions[prop] = options[prop];
				}
			}
			this.options.icon = new L.Icon(iconOptions);

		} else if (parts && parts.length === 3) {
			var IconClass = parts[1] === 'target' ? L.Icon.SmTarget : L.Icon.SmLocation;
			this.options.icon = new IconClass({color: parts[2]});
		} else {
			var iconType = type || 'default';

			iconOptions.iconUrl = L.Icon.Sputnik.imagePath + '/' + iconType + '.png';
			iconOptions.shadowUrl = L.Icon.Sputnik.imagePath + '/shadow.png';

			this.options.icon = new L.Icon.Sputnik(iconOptions);
		}
	}

});

function preparePopup(domain) {
	var html = '';

	if (domain && domain.title) {
		html += '<div class="sm-popup-title">' + domain.title + '</div>';
	}

	if (domain && domain.description) {
		html +=	'<div class="sm-popup-description">' + domain.description + '<div>';
	}

	return html;
}

function marker (latlng, options) {
	return new Marker(latlng, options);
}

module.exports = {
	Marker: Marker,
	marker: marker,
	preparePopup: preparePopup
};
