var L = require('./leaflet');

var Cluster = L.MarkerClusterGroup.extend({
	options: {
		showCoverageOnHover: true,
		polygonOptions: {
			"color": "#1e98ff",
			"weight": 0.5
			// "fill-opacity": 2,
			// "opacity": .9
			// "dashArray": "3, 5"
		}
	}
});

function cluster (options) {
	return new Cluster(options);
}

module.exports =  {
	Cluster: Cluster,
	cluster: cluster
};
