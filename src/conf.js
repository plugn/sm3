var smConf = {
	apiVersion: '0.3',
	// tilesURL: 'http://tiles.maps.sputnik.ru/tiles/kmt2/{z}/{x}/{y}.png?from_api=v{apiVersion}{urlParams}',
	tilesURL: '//tilessputnik.ru/{z}/{x}/{y}.png?from_api=v{apiVersion}{urlParams}',
	retinaTilesURL: '//tilessputnik.ru/{z}/{x}/{y}.png?tag=retina&from_api=v{apiVersion}{urlParams}',
	mapClassName: 'sputnik-maps',
	mapDefaults: {
		attributionControl: false,
		center: [55.75, 37.6167],
		zoom: 9,
		minZoom: 0,
		maxZoom: 19
	},
	routeURL: {
		car: 'http://routes.maps.sputnik.ru/osrm/router/viaroute',
		walk: 'http://footroutes.maps.sputnik.ru'
	}
};

module.exports = smConf;