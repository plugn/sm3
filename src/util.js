/*
 tries to get deep nested property of passed object,
 returns found returns it, else returns `def` argument

 getNested({p:{a:{t:{h:[1],i:[2]}}}}, 'p.a.t.h', '*')
 [1]
 getNested.call({p:{a:{t:{h:[1],i:[2]}}}}, 'p.a.t.i', '*' )
 [2]
 getNested({p:{a:{t:{h:[1],i:[2]}}}}, 'p.a.t.y')
 false
 */
function extract(root, path, def){
	var key,
		val = !!root ? root : this,
		arr = String(path).replace(/'|"|\]/g,'').replace(/\[/g,'.').split('.');
	while ((key = arr.shift()) && 'object' == typeof val && val) {
		val = 'undefined' == typeof val[key]? ('undefined' == typeof def? false : def) : val[key];
	}
	return val;
}

// Function.prototype.bind() micro implementaion
function bind(fn, context){
	var args = [].slice.call(arguments, 2);
	return function(){
		fn.apply(context, args.concat([].slice.call(arguments)));
	};
}

function log() {
	if (!(console && 'function' == typeof console.log)) {
		return;
	}
	console.log.apply(this, [].slice.call(arguments));
}

module.exports = {
	bind: bind,
	extract: extract,
	log: log
};
