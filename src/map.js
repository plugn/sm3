var L = require('./leaflet');
var smConf = require('./conf');

var Map = L.Map.extend({
	initialize: function (element, options) {
		var apiKey = L && L.sm && (L.sm.apiKey || L.sm.apikey);
		var protocol = ('' + location.protocol).indexOf('https') === 0 ? '' : 'http:';
		var params = {
			apiVersion: smConf.apiVersion,
			urlParams: apiKey ? '&apikey=' + apiKey : '',
			minZoom: smConf.mapDefaults.minZoom,
			maxZoom: smConf.mapDefaults.maxZoom
		};

		var tilesURL = L.Browser.retina ? smConf.retinaTilesURL : smConf.tilesURL;
		var tileLayer = L.tileLayer(protocol + tilesURL, params);
		options = L.Util.extend({layers: [ tileLayer ]}, smConf.mapDefaults, options || {});

		L.Map.prototype.initialize.call(this, element, options);
	}
});

Map.addInitHook(function () {
	if (this._container) {
		this._container.className += ' ' + smConf.mapClassName;
	}
});

Map.addInitHook(
	function onZoomChange() {
		var elCopyOsm,
			elCopyNe,
			curOsmShown;
		this.on('zoomend', function () {
			var zoom = this.getZoom();
			elCopyOsm = elCopyOsm || this.getContainer().querySelector('.js-sm-copyright-osm');
			elCopyNe = elCopyNe || this.getContainer().querySelector('.js-sm-copyright-ne');

			if (elCopyOsm && elCopyNe) {
				var needToShowOsm = zoom > 6;
				if (needToShowOsm !== curOsmShown) {
					curOsmShown = needToShowOsm;
					elCopyOsm.style.display = needToShowOsm ? '' : 'none';
					elCopyNe.style.display = needToShowOsm ? 'none' : '';
				}
			}
		});
	}
);


function map(id, options) {
	return new Map(id, options);
}

module.exports = {
	Map: Map,
	map: map
};
