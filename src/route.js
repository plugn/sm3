var conf = require('./conf');
var util = require('./util');
var L = require('./leaflet');
var jsonp = require('../libs/jsonp/jsonp');
var polyline = require('../libs/polyline/dist/polyline');


/**
 *
 * @param locations		[{L.LatLng}]
 * @param type		{String}
 * 		'car','walk','rapid'
 * @param options	{Object}
 */
function route (locations, type, options) {
	options = options || {};

	if (type in conf.routeURL) {
		requestRoute(locations, type, options.callback);
	} else if (!type || 'rapid' == type) {
		var result = {
				_layers: [],
				_gaps:[]
			};

		var routeLine = L.polyline(locations);
		if (routeLine) {
			result._layers.push(routeLine);
		}

		var request = {locations: locations, type: type},
			response = {},
			DSL = getDSL(result, response, request);

		if (options.callback) {
			options.callback(DSL);
		} else {
			return DSL;
		}
	}
}

function prepareLocations(locations) {
	var locs = [], loc;
	for (var i=0; i < locations.length; i++) {
		loc = locations[i];
		if (loc && loc.lat && loc.lng) {
			locs.push('loc='+loc.lat+','+loc.lng);
		}
	}
	return locs;
}

function prepareURL(locations, type) {
	var locs = prepareLocations(locations, type);
	return conf.routeURL[type] + '?' + locs.join('&') + '&alt=true&instructions=true';
}

function getGap(start, end) {
	var layer = L.polyline([start, end]),
		distance = L.latLng(start).distanceTo(end);
	return {
		latLngs: [start, end],
		layer: layer,
		distance: distance
	};
}

function getDSL(result, response, request) {
	return {
		getRequest: function() {
			return request;
		},
		getResponse: function () {
			return response;
		},
		getLayers: function () {
			return result._layers;
		},
		getLayer: function (index) {
			return result._layers[index];
		},
		getGaps: function () {
			return result._gaps;
		}
	};

}

function prepareResult(response, request, next) {
	var result = {
		_layers: [],
		_gaps:[]
	};

	var geo = util.extract(response, 'route_geometry'),
		mainLatLngs, mainLayer;
	if (geo) {
		mainLatLngs = polyline.decode(geo, 6);
		if (mainLatLngs && mainLatLngs.length) {
			mainLayer = L.polyline(mainLatLngs);
		}
		if (mainLayer) {
			response.route_geometry_decoded = mainLatLngs;
			result._layers.push(mainLayer);
		}
	}

	response.alternative_geometries_decoded = [];
	var altGeo = util.extract(response, 'alternative_geometries'), altLatLngs, altLayer;
	if (altGeo && altGeo.length) {
		for (var i = 0; i < altGeo.length; i++) {
			altLatLngs = polyline.decode(altGeo[i], 6);
			if (altLatLngs && altLatLngs.length) {
				altLayer = L.polyline(altLatLngs);
			}
			if (altLayer) {
				response.alternative_geometries_decoded.push(altLatLngs);
				result._layers.push(altLayer);
			}
		}
	}

	// gaps
	var start, end;
	if (response.via_points && request.locations) {
		for (var j = 0; j < Math.min(response.via_points.length, request.locations.length); j++ ) {
			start = request.locations[j];
			end = L.latLng(response.via_points[j]);
			if (start && end) {
				result._gaps.push(getGap(start, end));
			}
		}
	}

	next(getDSL(result, response, request));
}



function requestRoute (locations, type, callback) {
	if (!(type in conf.routeURL)) {
		throw new Error('(!) route::request() type illegal: ', type);
	}

	jsonp({
		url: prepareURL(locations, type),
		success: function (response) {
			var request = {locations: locations, type: type};
			return prepareResult(response, request, callback);
		},
		cbParamName: 'jsonp'
	});
}


module.exports =  route;
