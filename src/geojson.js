var L = require('./leaflet');
var marker = require('./marker').marker;

function geoJson (json, options) {
	options = options || {};

	var geoJsonOptions = L.extend({}, options, {
		// TODO: add documentation
		onEachFeature: function(geojson, layer) {
			geojson.style = options.style || geojson.style || options.defaultStyle || null;
			geojson.popupTemplate = options.popupTemplate || geojson.popupTemplate || options.defaultPopupTemplate || null;
			geojson.properties = L.Util.extend({}, options.defaultProperties, geojson.properties, options.properties);
			if ( options && options.onEachFeature ) {
				options.onEachFeature(geojson, layer);
			}
		}
	});

	// user defined pointToLayer support
	if (!options.pointToLayer) {
		geoJsonOptions.pointToLayer = function(geojson, latlng) {
			return marker(latlng, geojson.properties);
		};
	}


	return L.geoJson.css(json, geoJsonOptions);
}

module.exports = geoJson;
