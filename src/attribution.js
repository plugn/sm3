var L = require('./leaflet');
var Map = require('./map').Map;
var delimiterHtml = '<span class="copyrights-delimiter"> | </span>';

var Attribution = L.Control.Attribution.extend({
	options: {
		prefix:'<a href="http://maps.sputnik.ru/" target="_blank">Спутник</a>' + delimiterHtml + '<span>&copy; Ростелеком </span>',
		delimiter: delimiterHtml,
		leadingAttribs: '' +
			'<span class="js-sm-copyright-ne" style="display:none;">Natural Earth</span>' +
			'<span class="js-sm-copyright-osm" style=""><a href="http://www.openstreetmap.org/copyright" target="_blank">&copy; Openstreetmap</a></span>'
	},

	onAdd:	function (map) {
		L.Control.Attribution.prototype.onAdd.call(this, map);
		L.DomUtil.addClass(this._container, 'copyrights-control');

		return this._container;
	},

	_update: function () {
		if (!this._map) { return; }

		var attribs = [].concat(this.options.leadingAttribs);
		for (var i in this._attributions) {
			if (this._attributions[i]) {
				attribs.push('<span>' + i + '</span>');
			}
		}

		var prefixAndAttribs = [];

		if (this.options.prefix) {
			prefixAndAttribs.push('<span>' + this.options.prefix + '</span>');
		}
		if (attribs.length) {
			prefixAndAttribs.push(attribs.join(this.options.delimiter));
		}

		this._container.innerHTML = prefixAndAttribs.join(this.options.delimiter);
	}

});

Map.addInitHook(function () {
	this.attributionControl = (new Attribution()).addTo(this);
});

module.exports =  Attribution;
