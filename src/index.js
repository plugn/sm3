// JS API 3 entry point
(function () {
'use strict';

var L = require('./leaflet');
var map = require('./map');
var marker = require('./marker');
var geojson = require('./geojson');
var cluster = require('./cluster');
var button = require('./button');
var route = require('./route');
var geocoder = require('./geocoder/index');

require('./attribution');

L.sm = {
	Map: map.Map,
	map: map.map,

	Marker: marker.Marker,
	marker: marker.marker,

	geoJson: geojson,

	Cluster: cluster.Cluster,
	cluster: cluster.cluster,

	Button: button.Button,
	button: button.button,

	Geocoder: geocoder.Geocoder,
	geocoder: geocoder.geocoder,

	route: route
};

module.exports = L;

})();
