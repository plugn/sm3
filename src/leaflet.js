var L = require('../libs/leaflet/dist/leaflet-src'); // v0.7.7
require('../libs/Leaflet.geojsonCSS/leaflet.geojsoncss.js');
require('../libs/leaflet.markercluster/dist/leaflet.markercluster-src.js');
require('../libs/leaflet-button/L.Control.Button.js');

module.exports = L;
