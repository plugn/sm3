var L = require('./leaflet');

L.Icon.Sputnik = L.Icon.extend({
	options: {
		iconSize: [31, 41],
		iconAnchor: [16, 40],
		shadowAnchor: [4, 29],
		shadowSize: [32, 31],
		popupAnchor: [0, 3],
		className: 'leaflet-sm-marker'
	},

	_getIconUrl: function (name) {
		var key = name + 'Url';
		if (this.options[key]) {
			return this.options[key];
		}

		var path = L.Icon.Sputnik.imagePath;
		if (!path) {
			throw new Error(' (!) Couldn\'t autodetect L.Icon.Sputnik.imagePath, set it manually');
		}

		if ('icon' === name) {
			return path + '/default.png';
		}
	}
});

L.Icon.SmLocation = L.Icon.extend({
	options: {
		color: '',
		iconSize: [23, 31],
		iconAnchor: [11, 31]
	},
	_getIconUrl: function (name) {
		var color = this.options.color ? '-' + this.options.color : '';
		var key = name + color + 'Url';
		if (this.options[key]) {
			return this.options[key];
		}

		var path = L.Icon.Sputnik.imagePath;
		if (!path) {
			throw new Error(' (!) Couldn\'t autodetect L.Icon.Sputnik.imagePath, set it manually');
		}

		return path + '/location' + color + '.svg';
	},
	createShadow: function () {
		return null;
	}
});


L.Icon.SmTarget = L.Icon.extend({
	options: {
		color: '',
		iconSize: [23, 23],
		iconAnchor: [11, 13]
	},
	_getIconUrl: function (name) {
		var color = this.options.color ? '-' + this.options.color : '';
		var key = name + color + 'Url';
		if (this.options[key]) {
			return this.options[key];
		}

		var path = L.Icon.Sputnik.imagePath;
		if (!path) {
			throw new Error(' (!) Couldn\'t autodetect L.Icon.Sputnik.imagePath, set it manually');
		}

		return path + '/target' + color + '.svg';
	},
	createShadow: function () {
		return null;
	}
});


L.Icon.Sputnik.imagePath = (function () {
	var scripts = document.getElementsByTagName('script'),
		smScriptRe = /[\/^]sputnik_maps[\-\._]?([\w\-\._]*)\.js\??/;

	var i, len, src, path;

	for (i = 0, len = scripts.length; i < len; i++) {
		src = scripts[i].src;

		if (src.match(smScriptRe)) {
			path = src.split(smScriptRe)[0];
			path = path ? path + '/' : '';
			if (L.Icon && L.Icon.Default) {
				L.Icon.Default.imagePath = path + 'images';
			}
			return path + 'styles/images';
		}
	}
}());
