var L = require('./../leaflet'),
	Control = require('./control'),
	Sputnik = require('./geocoders/sputnik');

L.Util.extend(Control.class, {
	Sputnik: Sputnik.class,
	sputnik: Sputnik.factory
});

module.exports = L.Util.extend(L.Control, {
	Geocoder: Control.class,
	geocoder: Control.factory
});
