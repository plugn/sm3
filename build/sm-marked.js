var marked = require('marked');

/**
 * @title  docs markdown renderer for API.SPUTNIK.RU
 * @description sets formatting CSS classes for certain blocks of markdown
 * @example
<pre class="code"><code class="language-markup"> html </code></pre>
<pre class="code"><code class="language-javascript"> var json = {}; </code></pre>
<h1 class="api-title">JavaScript API Спутник/Карты</h1>
<h2 class="api-sub-title">Дополнительная информация</h2>
<p class="b-text_small"></p>
<ul class="b-api-ul"></ul> links

A hack for floats. Put in MD list-item this: `[//]: # (list.class=b-api-ul_float)`
<ul class="b-api-ul b-api-ul_float"></ul> for float lists
*/

function markedRenderer() {
	function escape(html, encode) {
		return html
			.replace(!encode ? /&(?!#?\w+;)/g : /&/g, '&amp;')
			.replace(/</g, '&lt;')
			.replace(/>/g, '&gt;')
			.replace(/"/g, '&quot;')
			.replace(/'/g, '&#39;');
	}

	var decodeLang = function (lang) {
		var aliases = {
			'js':'	javascript',
			'html':	'markup'
		};
		return (lang in aliases ? aliases[lang] : 'markup').replace(/\s+/mg, '');
	};

	var myRenderer = new marked.Renderer();

	myRenderer.code = function(code, lang, escaped) {
		if (this.options.highlight) {
			var out = this.options.highlight(code, lang);
			if (out != null && out !== code) {
				escaped = true;
				code = out;
			}
		}

		if (!lang) {
			return '<pre class="code"><code class="language-markup">'
				+ (escaped ? code : escape(code, true))
				+ '\n</code></pre>';
		}

		return '<pre class="code"><code class="language-'
			+ decodeLang(escape(lang, true))
			+ '">'
			+ (escaped ? code : escape(code, true))
			+ '\n</code></pre>\n';
	};

	myRenderer.paragraph = function(text) {
		return '<p class="b-text_small">' + text + '</p>\n';
	};

	myRenderer.heading = function(text, level, raw) {
		level = +level;
		return '<h'
			+ level
			+ (1 === level ? ' class="api-title"' : (2 === level ? ' class="api-sub-title"' : ''))
			+ '>'
			+ text
			+ '</h'
			+ level
			+ '>\n';
	};

	myRenderer.list = function(body, ordered) {
		var classNames = ['b-api-ul'];
		if (/images\/[\w_-]+\.svg|png|jpe?g/.test(body)) {
			classNames.push('b-api-ul_float');
		}

		var extra = ' class="' + classNames.join(' ') + '"',
			type = ordered ? 'ol' : 'ul';

		return '<' + type + extra + '>\n' + body + '</' + type + '>\n';
	};

	return myRenderer;
}

marked.setOptions({
	renderer: markedRenderer(),
	gfm: true,
	langPrefix: 'language-',
	tables: true,
	breaks: false,
	pedantic: false,
	sanitize: false,
	smartLists: true,
	smartypants: true
});

module.exports = marked;
