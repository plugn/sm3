'use strict';

// Include gulp & tools we'll use
var util = require('util');
var gulp = require('gulp');
var $ = require('gulp-load-plugins')();
var del = require('del');
var runSequence = require('run-sequence');
var browserSync = require('browser-sync');
var reload = browserSync.reload;
var source = require('vinyl-source-stream');
var browserify = require('browserify');
var merge2 = require('merge2');
var fileinclude = require('gulp-file-include');
var marked = require('./sm-marked'); // Sputnik Maps mode for `marked`

// configuration
var distPath = '../dist';
var docSrc = '../app/docs';
var srcJsGlob = ['../src/**/*.js', docSrc + '/**/*.js'];
var srcStaticGlob = ['../{src,app,test}/**/*.{md,html,js,scss,css}'];

// Lint JavaScript and reload browsers
gulp.task('jshint', function () {
	return gulp.src(srcJsGlob)
		.pipe($.jshint())
		.pipe($.jshint.reporter('jshint-stylish'))
		.pipe($.if(!browserSync.active, $.jshint.reporter('fail')));
});

gulp.task('loader', function () {
	return gulp.src(['../app/loader/loader.js'])
		.pipe($.uglify({preserveComments:'license'}))
		.pipe(gulp.dest(distPath));
});

gulp.task('test', function () {
	return gulp.src('../test/**/*')
		.pipe(gulp.dest(distPath + '/test'))
		.pipe($.size({title: 'test'}));
});

// Copy all non-html files at the root level (src)
gulp.task('copy', function () {
	return gulp.src([
		'../src/*',
		'!../src/*.html'
	], {
		dot: true
	}).pipe(gulp.dest(distPath))
		.pipe($.size({title: 'copy'}));
});

gulp.task('images', function () {
	return gulp.src('../src/styles/images/**/*')
		.pipe(gulp.dest(distPath + '/styles/images'))
		.pipe($.size({title: 'images'}));
});

gulp.task('docs-images', function () {
	return gulp.src(docSrc + '/images/**/*')
		.pipe(gulp.dest(distPath + '/docs/images'))
		.pipe($.size({title: 'docs-images'}));
});

gulp.task('docs-include', function () {
	return gulp.src(docSrc + '/*.html')
		.pipe( fileinclude({
				prefix: '@@',
				basepath: '@file',
				filters: {
					markdown: marked.parse
				},
				context: {
					clusters: false
				}
			}))
		.pipe( gulp.dest(distPath + '/docs'));
});

gulp.task('docs-html', function () {
	return gulp.src([
		docSrc + '/examples/*.{html,js}'
	])
		.pipe(gulp.dest(distPath + '/docs/examples'));
});

gulp.task('docs-styles', function () {
	return gulp.src([
		docSrc + '/**/*.{scss,css}'
	])
		//.pipe($.changed(distPath, {extension: '.css'}))
		.pipe($.sass({
			precision: 10
		}).on('error', $.sass.logError))
		.pipe($.concat('docs.min.css'))
		.pipe($.sourcemaps.init())
		.pipe($.if('*.css', $.minifyCss()))
		.pipe($.size({title: 'docs.min.css'}))
		.pipe($.sourcemaps.write('.'))
		.pipe(gulp.dest(distPath + '/styles'));
});

gulp.task('docs', ['docs-include', 'docs-html', 'docs-styles']);

gulp.task('html', function () {
	return gulp.src([
			'../app/*.html'
		])
		.pipe(gulp.dest(distPath));
});

// Compile stylesheets
gulp.task('styles', function () {
	var appStream = gulp.src(['../src/styles/**/*.{scss,css}'])
		.pipe($.changed(distPath, {extension: '.css'}))
		.pipe($.sass({
			precision: 10
		}).on('error', $.sass.logError))
		.pipe($.concat('sputnik_maps.css'))
		.pipe(gulp.dest(distPath))
		.pipe($.size({title: 'sputnik_maps.css'}));

	var libStream = gulp.src([
		'../libs/leaflet/dist/**/*.css',
		'../libs/leaflet-control-geocoder/*.css',
		'../libs/leaflet.markercluster/dist/*.css'
	]);

	return merge2(libStream, appStream)
		.pipe($.concat('sputnik_maps_bundle.min.css'))
		.pipe($.sourcemaps.init())
		.pipe($.if('*.css', $.minifyCss()))
		.pipe($.size({title: 'sputnik_maps_bundle.min.css'}))
		.pipe($.sourcemaps.write('.'))
		.pipe(gulp.dest(distPath));
});

// Concatenate and minify JavaScript
gulp.task('browserify', function () {
	return browserify('../src/index.js', {
			'basedir': '../src'
		})
		.bundle()
		.pipe(source('sputnik_maps_bundle.js'))
		.pipe(gulp.dest(distPath))
		.pipe($.streamify($.sourcemaps.init()))
		.pipe($.streamify($.size({title: 'sputnik_maps_bundle.js'})))
		.pipe($.streamify($.uglify({preserveComments:'license'})))
		.pipe($.rename('sputnik_maps_bundle.min.js'))
		.pipe($.streamify($.size({title: 'sputnik_maps_bundle.min.js'})))
		.pipe($.streamify($.sourcemaps.write('.')))
		.pipe(gulp.dest(distPath));
});

gulp.task('lib-leaflet-images', function () {
	var sources = ['../libs/leaflet/dist/images/*', '../libs/leaflet-control-geocoder/images/*'];
	return gulp.src(sources)
		.pipe(gulp.dest(distPath + '/images'));
});

gulp.task('libs', ['lib-leaflet-images']);

// Clean output directory
gulp.task('clean', del.bind(null, [distPath + '/*', '!' + distPath + '/.git'], {dot: true, force: true}));

gulp.task('rebuildStaticAndReload', function(cb) {
	runSequence('rebuildStatic', 'reload', cb);
});

gulp.task('rebuildJSAndReload', function(cb) {
	runSequence('rebuildJS', 'reload', cb);
});

// Build and serve the output from the dist build
gulp.task('serve', ['default'], function () {
	browserSync({
		notify: false,
		reloadOnRestart: true,
		open: false,
		logPrefix: 'SMJ',
		server: {
			baseDir: distPath
		}
	});
	gulp.watch(srcStaticGlob, ['rebuildStaticAndReload']);
	gulp.watch(srcJsGlob, ['rebuildJSAndReload']); // JS serve
});

gulp.task('rebuildStatic', ['loader', 'docs', 'html', 'styles', 'images', 'test'], function (cb) {
	return cb();
});

gulp.task('rebuildJS', ['jshint', 'browserify'], function (cb) {
	return cb();
});

// Build production files, the default task
gulp.task('default', ['clean'], function (cb) {
	runSequence(
		'libs',
		['rebuildJS','rebuildStatic'],
		// 'reload',
		cb);
});

gulp.task('reload', function(cb){
	reload();
	return cb();
});

