# Собирается в дженкинсе так:
# #!/bin/bash
# ###### Устанавливаем нужные переменные окружения
# # Версия CentOS
# export OS_RELEASE=6.5
# # Название репозитория
# export REPO_NAME="frontend"
#
# git log -n1
#
# source /home/jenkins/.rvm/scripts/rvm
# export PATH="$PATH:/home/jenkins/.rvm/bin"
# rvm use 2.2.1
#
# # Запускаем сборку
# if [[ ! -z $Version ]]; then
# 	/usr/local/bin/rpmbuild.pl --name sips_maps_js_api --version ${Version}
# else
# 	/usr/local/bin/rpmbuild.pl --name sips_maps_js_api
# fi
#
# # Выводим, что получилось, в консоль
# rpm -qlp .rpm/RPMS/noarch/*.rpm
#
# # Подписываем пакет
# /usr/local/bin/sign .rpm/RPMS/noarch/*.rpm
#
# # Переносим пакет в девелоперский репозиторий
# #python /usr/local/bin/add_package_to_repo.pyc --centos_release $OS_RELEASE --repo_name $REPO_NAME --repo_type dev --packages $(ls -1 .rpm/RPMS/noarch/*.rpm)
# cd .rpm/RPMS/noarch/
#
# for package in $(ls -1 *.rpm)
# do
#     /usr/local/bin/push2yum $REPO_NAME dev $package
#     #move2test $REPO_NAME dev $package
# done

%define api_version 0.3

Name:           sips_maps_js_api_%{api_version}
Version:        %{ver}
Release:        1.kmm6
Summary:        Maps UI
Buildarch:      noarch
Group:          Applications/Internet
License:        Proprietary
AutoReqProv:    no
BuildRoot:      /%{_tmppath}/%{name}-%{version}-root
Prefix:         /kmsearch

%description
Maps Js API %{api_version}

%build
cd sips_maps_js_api-%{version}

path_v=./__release_versions
mkdir $path_v

rm -rf $path_v/v%{api_version}
mkdir $path_v/v%{api_version}

cd ./build
rm -rf ./node_modules

# git clone http://gitlab.srv.pv.km/bolgov/maps-jsapi-node-modules.git node_modules
# it fails on buildserver because of no internet there
npm install #>/dev/null

# ./node_modules/gulp/bin/gulp.js prepare --js-api-version=v%{api_version} #>/dev/null
./node_modules/gulp/bin/gulp.js default #>/dev/null
cd -

cp -r ./dist/* $path_v/v%{api_version}

# write version
echo '{"name": "sips_maps_js_api", "version_pkg": "%{ver}", "version_api": "%{api_version}"}' > $path_v/v%{api_version}/version.json


%install
DST=%{buildroot}%{prefix}/maps_js_api

rm -rf %{buildroot}
mkdir -p $DST
cp -r sips_maps_js_api-%{version}/__release_versions/* $DST

%clean
#rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
/*
